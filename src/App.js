import React from 'react';
import './App.css';

import PokemonList from "./components/pokemon_list";

function App() {
  return (
    <div className="App">
      <header className="App-header">
      <PokemonList></PokemonList>
      </header>
    </div>
  );
}

export default App;
